//百度来的
function creatObjectURL(file) {
    if (window.URL) {
        return window.URL.createObjectURL(file);
    } else if (window.webkitURL) {
        return window.webkitURL.createObjectURL(file);
    } else {
        return null
    }
}

function convertBase64UrlToBlob(urlData){

    var bytes=window.atob(urlData.split(',')[1]);        //去掉url的头，并转换为byte

    //处理异常,将ascii码小于0的转换为大于0
    var ab = new ArrayBuffer(bytes.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < bytes.length; i++) {
        ia[i] = bytes.charCodeAt(i);
    }

    return new Blob( [ab] , {type : 'image/png'});
}

//验证
function verificate(data){
    var error = ''
    //验证图片，用地址吧
   if(!data.url){
       error = "请输入图片"
       
   }
   else if(!data.name){
      error = "请输入食品名称"
       
   }
  else if(!data.name){
     error = "请输入食品名称"
    
   }
  else if(!data.location){
     error = "请输入店铺位置"
     
   }
   else if(!data.price){
    error = "请输入价格"
    
   }else{
      if( !/^\d+$/.test(data.price) ){
        error = "请输入数字"
      }
   }
   return error
}

const base_path = 'http://food-api.twtstudio.com'
//请求数据
function request(){
    var currentSchool = this.state.currentSchool
    //可以加加载页面
    //
    $.get(`${base_path}/api/image?campus=${currentSchool}`,
           function(res){
               console.log(res)
                var message = res.message
                switch(message){
                    case "请求成功":
                         render(res.data)
                         break;
                    default:
                        window.alert(message)
                        break;
                }
            });
}

//上传数据
function post(){
    var data = {
        price : $("#price").val(),
        name  : $("#store-name").val(),
        location : $("#store-adress").val(),
        introduction : $("#store-desc").val()?$("#store-desc").val():"暂无介绍",
        url :  $("#file").val(),
        campus : state.currentSchool
    }
    var error = verificate(data)
    console.log( error )
    if( error ) { 
         $(".input-error").text(error).removeClass("hide")
           return
        }

    var image = $("#file").prop('files')[0]
    var url = creatObjectURL(image)
    //将地址给data.url
    data.url = url
    console.log( image )
   
   
    var formData = new FormData(); 
    
    formData.append("image", image )
    formData.append("price", data.price)
    formData.append("name", data.name)
    formData.append("location", data.location)
    formData.append("introduction", data.introduction)
    formData.append("campus", data.campus)
   
     //切到加载页
     render(data)
    $.ajax({url:`${base_path}/api/image`,
             type: "POST",
             data : formData,
             processData: false,
             dataType: false,
             contentType: false,
             success:function(res){
                console.log(res)
                var err = res.err
                switch(err){
                    case 0:
                         render(data)
                         console.log(data)
                         console.log("render data")
                         break;
                    default:
                        window.alert(message)
                        break;
                }
         }});
   
    //console.log(`image=${file}&&campus=${state.currentSchool}&&name=${store_name}&&location=${store_adress}&&price=${price}&&introduction=xxx `)
}

//在结果页渲染
function render(data){
    // var img = new Image();
    // img.crossOrigin = "Anonymous";
    // img.class = "content2-img"
    // img.src = data.url
    $(".content2-name").text(data&&data.name?data.name:"未知")
    $(".content2-place").text(data&&data.location?data.location:"未知")
    // $(".content2-img-container").empty().append(img)
     $(".content2-img").attr("src", data.url)
    $(".content2-price").text(data&&data.price?data.price:"未知");
    $(".content2-description").text(data&&data.introduction?data.introduction:"");
    //渲染完跳转到结果面
    console.log(".....loading....")
    state.switchPage(1)
}

var state = {
    currentSchool: 0,  //学校的序号，0 是北洋园， 1 是卫津路
    count: 0, 
    maxCount: 5,         //计算加载次数
    currentPageNumber:0, //当前页面的序号
    //切换学校
    switchSchool: function switchSchool(){
                      this.currentSchool = (this.currentSchool+1)%2;
                      console.log("school is: " + this.currentSchool)
                      switch (this.currentSchool) {
                          case 0:
                              $(".uer-interact .school-container .school")
                                   .empty()
                                   .append("北洋园")
                                   .css("background","rgb(255, 0, 0)")
                                   .css("box-shadow","15px 15px 0px rgb(77, 73, 245)")
                              break;
                          case 1:
                               $(".uer-interact .school-container .school")
                                   .empty()
                                   .append("卫津路")
                                   .css("background","rgb(77, 73, 245)")
                                   .css("box-shadow","15px 15px 0px rgb(255, 0, 0)")
                                break;
                          default:
                              console.log("school is wrong")
                      }
                  },
    //切换页面,传入pageNumber: 0代表第一页，1代表结果页，
    //       2代表询问是否上传, 3是上传页, 4是加载页面
    switchPage:  function switchPage(pageNumber){
              //zoomIn消失
              $(".buttons .button").addClass("hide");
              $(".content").addClass("hide");
              $(".content2").removeClass("blur")
              $(".tip").addClass("hide")
              switch (pageNumber) {
                  case 0 :                     
                      break;
                  case 1 :
                         $(".button2").removeClass("hide")
                         $(".button3").removeClass("hide")
                         $(".tip2").removeClass("hide")
                         $(".content2").removeClass("hide")
                         break;
                  case 2: 
                         //为结果页加上模糊，上面是询问
                         $(".content2").removeClass("hide").addClass("blur")
                        $(".button4").removeClass("hide")
                        $(".button5").removeClass("hide")
                        
                        $(".content3").removeClass("hide")
                        break;
                case 3: 
                        $(".input-error").addClass("hide")
                        $(".button6").removeClass("hide")
                        $(".tip3").removeClass("hide")
                        $(".content4").removeClass("hide")
                        break;
                 case 4: 
                       //加载页
                       $(".loading").removeClass("hide")
                  default:
                      break;
              }
    },
    //增加add, 如果小于10跳转到结果页，达到10跳到询问是否不满意的页面
    addCount: function addCount(){
              this.count ++;
              if(this.count < this.maxCount){
                  //  请求
                  request()
                  state.switchPage(4)
              }else{
                  state.switchPage(2)
                  this.count = 0;
              }
    }
    
}


/**点击切换学校 */
$(".school")
   .click( state.switchSchool.bind(state));

/**点击拍板 */
$(".button1").click( ()=>{ 
                console.log(`click`)
               state.addCount()
                console.log( state.count )
             });
/**点击换个口味 */
$(".button2").click( ()=>{ 
   
    state.addCount()
     console.log( state.count )
  });
  /**点击更多好吃的 */
  $(".button3").click( ()=>{ 
    state.switchPage(3)
    
     console.log( state.count )
  });
/**点击我再看看 */
$(".button4").click( ()=>{ 
   
    state.addCount()
     console.log( state.count )
  });
  /**不啬赐教 */
  $(".button5").click( ()=>{   
    state.switchPage(3)
     console.log( state.count )
  });
  /**点击确认 */
$(".button6").click( ()=>{ 
    post()
  });

  /***点击喜欢/截屏 */
  $(".content2-like").click( ()=>{
      getPicture()
  })
   
  /**照片图换成完成图 */
  $("#file").change( ()=>{
      $(".camera").addClass("complete")
  })
function getPicture(){
      // 把button换成二维码
      $(".share_picture").removeClass("hide")
      //加载
      $(".share_loading").removeClass("hide")
      $(".loading").removeClass("hide")
      $(".buttons .button").addClass("hide");
      $(".erweima").removeClass("hide")
      $(".background-rotate").removeClass("rotate")
     // 取消zoomIn和fadeIn动画, 防止影响截图
    html2canvas($(".weibo-h5")[0],{windowWidth:1080, height:1920, useCORS:true, allowTaint: false}).then(function(canvas) {
        $(".share_loading").addClass("hide")
        $(".mypic").attr("src", canvas.toDataURL("image/png") )
       })
      
    }


